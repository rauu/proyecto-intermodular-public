import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Server } from '../services/server';
import { ObservablesService } from '../services/observables.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../Clases/user';
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { SPACE } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
declare var $: any;


export interface Tag {
  name: string;
};

@Component({
  selector: 'app-nav-privado',
  templateUrl: './nav-privado.component.html',
  styleUrls: ['./nav-privado.component.css']
})

export class NavPrivadoComponent implements OnInit {

  description_text = ''
  userLog = new User;
  tagText = '';
  userText = '';
  allPosts = <any>[];


  uploadPhoto!: FormGroup;
  submitted = false;

  search_focus = false;



  //chips
  readonly separatorKeysCodes: number[] = [SPACE];
  //chips array
  tags: Tag[] = [

  ];




  constructor(public server: Server, public observables: ObservablesService, private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder) {

  }

  ngOnInit() {

    this.uploadPhoto = this.formBuilder.group({
      Image: [null, Validators.required],
      Description: [''],
      Tag: [''],
      Location: ['']

    });


    if (sessionStorage.getItem("username")) {

      this.server.read('user', sessionStorage.getItem("username"), '').subscribe(
        //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
        (res) => {
          if (sessionStorage.getItem("username") == res[0].username) {
            this.userLog = res[0];
          } else {
            this.router.navigate(['/login']);
          }
        },
        (err) => {
          this.router.navigate(['/login']);
        });

    } else {

      if (localStorage.getItem("username")) {

        this.server.read('user', localStorage.getItem("username"), '').subscribe(
          //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
          (res) => {
            if (localStorage.getItem("username") == res[0].username) {
              this.userLog = res[0];
            } else {
              this.router.navigate(['/login']);
            }
          },
          (err) => {
            this.router.navigate(['/login']);
          });

      } else {
        this.router.navigate(['/login']);
      }
    }

    //CARGAR TODOS LOS POSTS PARA TRABAJAR CON ELLOS
    this.server.read('post', '', 'all').subscribe(
      //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
      (res) => {
        this.observables.addPost(res);
      },
      (err) => {
        //console.log(err);
      });
  }

  get f() { return this.uploadPhoto.controls; }


  myFunction(): boolean {

    //let elements: NodeListOf<Element> = document.getElementsByClassName("form-inline");

    let classes: DOMTokenList = document.getElementsByClassName("form-inline")[0].classList;
    let classesNav: DOMTokenList = document.getElementsByClassName("nav")[0].classList;
    return classes.toggle("responsive"), classesNav.toggle("navbar-nav");

  }

  fileEvent(fileInput: any) {

    let file = fileInput.target.files[0];
    let p = document.getElementById("photo_name");
    if (file.type == "image/jpg" || file.type == "image/jpeg" || file.type == "image/png") {
      p!.innerHTML = "Photo Selected = " + file.name;
    } else {
      alert("Error las imagenes solo puedes subir en formato: jpeg, jpg o png");
      file = [];
      p!.innerHTML = "No Photo selected";
    }

  }

  tagSearch() {

    if (this.tagText == '') {

      this.server.read('post', '', 'all').subscribe(
        //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
        (res) => {

          this.observables.addPost(res);
        },
        (err) => {
          //console.log(err);
        });
    } else {

      this.server.toDo('tagSearch', '', this.tagText).subscribe(
        (res) => {
          this.observables.addPost(res);
        },
        (err) => {
          let array = <any>[];
          this.observables.addPost(array);
        });
    }
  }

  userSearch() {

    if (this.userText == '') {

      let array = <any>[];
      this.observables.addUser(array);

    } else {

      this.server.toDo('userSearch', '', this.userText).subscribe(
        (res) => {

          if (res == '0 resultados') {
            let array = <any>[];
            this.observables.addUser(array);
          } else {
            this.observables.addUser(res);
          }
        },
        (err) => {
          let array = <any>[];
          this.observables.addUser(array);
        });
    }
  }

  focus_search_true() {
    this.search_focus = true;
  }
  focus_search_false() {
    this.search_focus = false;
  }


  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim() && this.tags.length < 5) {
      this.tags.push({ name: value.trim() });

    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(tags: Tag): void {
    const index = this.tags.indexOf(tags);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }




  onSubmit(file: any) {
    this.submitted = true;


    // stop here if form is invalid
    if (this.uploadPhoto.invalid) {
      return;
    } else {
      $('.toast').toast('show');
      this.addPost(file);
      this.submitted = false;
      window.setTimeout(function () {
        window.location.reload();
      }, 1000);
    }
    //this.addEvent();
    // display form values on success
  }



  addPost(file: any) {
    //$username, $location, $image, $date, $description, $tag_Name
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let today = year + "-" + month + "-" + day;
    let tag: String = "";
    for (let x = 0; x < this.tags.length; x++) {
      tag += this.tags[x].name + " ";
    }

    //split para la extenxion
    let split = file.files[0].type;
    let extensions = split.split("/", 2);


    // console.log(tag);

    const reader: FileReader = new FileReader();
    reader.readAsDataURL(file.files[0]);
    reader.addEventListener('loadend', e => {
      let blopPhoto: any = reader.result as string;
      // this.server.insert("post", edd).subscribe();
      // createPost( $username, $location, $image, $id, $date, $description )
      let array = <any>[this.userLog.username, this.uploadPhoto.value.Location, this.uploadPhoto.value.Image, today, this.uploadPhoto.value.Description, tag, blopPhoto, extensions[1]];
      this.server.insert("post", array).subscribe();
    });




    //console.log(this.uploadPhoto.value);
    // console.log(array);
    //this.location.replaceState('/homeUser');
    //window.location.reload();
    //console.log(this.method.prueba);
    //insert
  }


  mostrar(file: any) {
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(file.files[0]);
    reader.addEventListener('loadend', e => {
      let edd: any = reader.result as string;
      // this.server.insert("post", edd).subscribe();

    });
  }

  logout() {
    sessionStorage.removeItem('username');
    localStorage.removeItem('username');
  }


}
