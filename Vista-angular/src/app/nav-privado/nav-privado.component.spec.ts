import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavPrivadoComponent } from './nav-privado.component';

describe('NavPrivadoComponent', () => {
  let component: NavPrivadoComponent;
  let fixture: ComponentFixture<NavPrivadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavPrivadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavPrivadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
