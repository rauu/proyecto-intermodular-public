import { Component, OnInit, HostListener, OnChanges, SimpleChanges } from '@angular/core';
import { Server } from '../services/server';
import { ObservablesService } from '../services/observables.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../Clases/user';
import { Comment } from '../Clases/comment';
import { Post } from '../Clases/post';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
})
export class PostComponent implements OnInit, OnChanges {

  userLog = new User;
  post = new Post;
  masPosts = [Post];
  likesTotales: any;
  liked: any;
  array: any;
  profileUserPost: any;
  usernameLocal: any;
  usernameSession: any;
  comment!: FormGroup;
  replay_comment!: FormGroup;
  submitted = false;
  mobile_900 = false;
  mobile_500 = false;
  complete_screen = true;
  replies = 0;

  constructor(public server: Server, public observables: ObservablesService, private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.replies){
      this.observables.getComments();
    }
  }



  @HostListener("window:resize", [])
  onResize() {
    var width = window.innerWidth;
    if(width < 900){
      this.complete_screen = false;
      this.mobile_900 = true;
      this.mobile_500 = false;
      //this.mobile_500 = false;
    }
    if(width < 600){
      //this.mobile_900 = false;
      this.complete_screen = false;
      this.mobile_500 = true;
      this.mobile_900 = false;
    }
    if(width > 900){
      this.complete_screen = true;
      this.mobile_500 = false;
      this.mobile_900 = false;
    }
  }



  ngOnInit(): void {

    this.onResize()

    this.observables.addComments([]);
    this.observables.addVar(3);



    this.server.read('post', '', this.route.snapshot.paramMap.get('id')).subscribe(
      //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
      (res) => {
        if (res == '0 resultados') {
          this.router.navigate(['/home']);
        } else {

          this.post = res[0];
          this.server.read('comments', '', this.post.id).subscribe(
            (res) => {
              this.observables.addComments(res);
            }
          );

          this.observables.addLikes(res[0].valMedia);

          this.server.read('post', this.post.username, '').subscribe(
            //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE

            (res) => {

              this.server.read('user', this.post.username, '').subscribe(
                (res) => {

                  this.server.read('value', sessionStorage.getItem("username") || localStorage.getItem("username"), this.route.snapshot.paramMap.get('id')).subscribe(
                    (res) => {
                      this.liked = res;

                      this.server.read('user', this.usernameLocal || this.usernameSession,'').subscribe(
                        (res) => {
                          if(res != '0 resultados'){
                            this.userLog = res[0];
                          }
                        }
                      );

                    });


                  this.profileUserPost = res[0].profile_image;
                });

              if (res != '0 resultados') {
                this.masPosts == res;
              };
            },
            (err) => {
              //console.log(err);
            });


        }
      },
      (err) => {
        //console.log(err);
      });

    this.comment = this.formBuilder.group({
      comment_data: ['', Validators.required]
    });
    this.replay_comment = this.formBuilder.group({
      comment_data: ['', Validators.required]
    });



this.usernameLocal = localStorage.getItem('username');
this.usernameSession = sessionStorage.getItem('username');
}
  get f() { return this.comment.controls; }
  get e() { return this.replay_comment.controls; }


  comprobarLog() {
    if (!sessionStorage.getItem("username") && !localStorage.getItem("username")) {
      return false;
    } else {
      return true;
    }
  }

  deletePost(id: any) {
    var check = confirm('Are you sure?');
    if (check == true) {
      this.server.delete('post', id).subscribe(
        (res) => {
          //console.log(res);
        },
        (err) => {
          //console.log(err);
        }
      );

    } else {

    }
    this.router.navigate(['home']);
  }

  uploadComment() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.comment.invalid) {
      //$('.no-post').toast('show');
      return;
    } else {
      //$('.post').toast('show');

      this.addComment();

      this.submitted = false;
      this.comment.reset();

      setTimeout(() => {
        window.location.reload();
      }, 500);
    }
  }

  addComment() {
    //$username,$parent_id,$comment,$id_post
    let user = sessionStorage.getItem("username") || localStorage.getItem("username");
    let array: any = [user, -1, this.comment.value.comment_data, this.post.id];

    this.server.insert('comment', array).subscribe(
      (res) => {
        this.server.read('comments', '', this.post.id).subscribe(
          (res) => {
            this.observables.addComments(res);
          }
        );
      }
    );
  }

  addReply(idComment:any) {
    //$username,$parent_id,$comment,$id_post
    let user = sessionStorage.getItem("username") || localStorage.getItem("username");
    let array: any = [user, idComment, this.replay_comment.value.comment_data, this.post.id];

    this.server.insert('comment', array).subscribe(
      (res) => {
        this.server.read('comments', '', this.post.id).subscribe(
          (res) => {
            this.observables.addReplies(res);
          }
        );
      }
    );
  }

  public show: boolean = false;

  toggle(idPost:any) {

    // CHANGE THE NAME OF THE BUTTON.
    if (this.show) {
      this.show = false;
    }
    else {
      this.comprobarReplies(idPost);
      this.show = true;
    }
  }

  replayComment(idComment:any) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.replay_comment.invalid) {
      //$('.no-post').toast('show');
      return;
    } else {
      //$('.post').toast('show');
      if (this.show) {
        this.show = false;
      }
      else {
        this.show = true;
      }

      this.addReply(idComment);
      this.submitted = false;
      this.replay_comment.reset();
    }
  }

  dislike() {

    this.array = this.route.snapshot.paramMap.get('id') + '-' + sessionStorage.getItem("username") || localStorage.getItem("username");

    this.server.delete('value', this.array).subscribe();

    this.server.read('post', '', this.route.snapshot.paramMap.get('id')).subscribe(
      (res) => {
        let editPost = res[0];
        this.array = [editPost.id, editPost.username, editPost.fecha, editPost.dateApproved, editPost.location, editPost.descripcion, editPost.imagen, (parseInt(editPost.valMedia) - 1)];
        this.server.update('post', this.route.snapshot.paramMap.get('id') || '', this.array).subscribe();
      }
    );

    this.liked = '0 resultados';
    this.observables.addLikes(parseInt(this.observables.getLikes()) - 1);
  }

  like() {

    this.array = [this.route.snapshot.paramMap.get('id'), sessionStorage.getItem("username") || localStorage.getItem("username"), 1];

    this.server.insert('value', this.array).subscribe();

    this.server.read('post', '', this.route.snapshot.paramMap.get('id')).subscribe(
      (res) => {
        let editPost = res[0];
        this.array = [editPost.id, editPost.username, editPost.fecha, editPost.dateApproved, editPost.location, editPost.descripcion, editPost.imagen, (parseInt(editPost.valMedia) + 1)];
        this.server.update('post', this.route.snapshot.paramMap.get('id') || '', this.array).subscribe(
          (res) => {
            this.server.insert('value', this.array).subscribe();
          }
        );
      }
    );

    this.liked = '';
    this.observables.addLikes(parseInt(this.observables.getLikes()) + 1);
  }

  profileimage(user: string) {/*
    this.server.read('user', user, '').subscribe(
      (res) => {
        console.log(res);
        //return res[0].profile_image;
        this.profileI =  res[0].profile_image;
      }
    );
    console.log(user);*/
  }

  profileI: any;

  resize(pic:any, imgW:number, imgH: number){

    let width = pic.naturalWidth;
    let height = pic.naturalHeight;

    while(width > imgW || height > imgH){
      width = width / 1.2;
      height = height / 1.2;
    }

    pic.style.width = width + 'px';
    pic.style.heigth = height + 'px';

  }

  innerWidth: any;
  innerHeight: any;
  responsiveImage(){
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;
    if(window.innerWidth < 800){

    }
  }

  comprobarReplies(id:any){

    this.server.read('comments', 'commentsReply', id).subscribe(
      (res) => {
        if(res == '0 resultados'){
          let comment = new Comment;
          comment.parent_id = id;
          this.observables.addReplies([comment]);
        }else{
          this.observables.addReplies(res);
        }
      }
    );
  }
}
