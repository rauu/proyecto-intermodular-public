import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewEncapsulation } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatTooltipModule} from '@angular/material/tooltip';





import { Post } from './Clases/post';
import { User } from './Clases/user';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { HomeComponent } from './home/home.component';
import { HomeUserComponent } from './home-user/home-user.component';
import { SettingComponent } from './setting/setting.component';
import { WaitingPostsComponent } from './waiting-posts/waiting-posts.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { UserComponent } from './user/user.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { NavPublicoComponent } from './nav-publico/nav-publico.component';
import { FooterComponent } from './footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavPrivadoComponent } from './nav-privado/nav-privado.component';
import { DropDirective } from './drop.directive';
import { PostModalComponent } from './post-modal/post-modal.component';
import { AdministradorUsuariosComponent } from './administrador-usuarios/administrador-usuarios.component';
import { InfoUserComponent } from './info-user/info-user.component';
import { PostComponent } from './post/post.component';


@NgModule({
  declarations: [

    AppComponent,
    LoginComponent,
    RegisterComponent,
    WelcomeComponent,
    HomeComponent,
    HomeUserComponent,
    SettingComponent,
    WaitingPostsComponent,
    ContactUsComponent,
    UserComponent,
    ForgetPasswordComponent,
    NavPublicoComponent,
    FooterComponent,
    NavPrivadoComponent,
    DropDirective,
    AdministradorUsuariosComponent,
    InfoUserComponent,
    PostModalComponent,
    PostComponent
    // warning: few browsers support shadow DOM encapsulation at this time

  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatTooltipModule,
    RouterModule.forRoot([
      {
        path: '',
        component: WelcomeComponent,
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'homeUser',
        component: HomeUserComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'register',
        component: RegisterComponent,
      },
      {
        path: 'settings',
        component: SettingComponent,
      },
      {
        path: 'waitingPost',
        component: WaitingPostsComponent,
      },
      {
        path: 'contactUs',
        component: ContactUsComponent,
      },
      {
        path: 'forgetPassword',
        component: ForgetPasswordComponent,
      },
      {
        path: 'administrator',
        component: AdministradorUsuariosComponent,
      },
      {
        path: ':username',
        component: InfoUserComponent,
      },
      {
        path: 'post/:id',
        component: PostComponent,
      },
      {
        path: '**',
        component: NotFoundComponent,
      }
    ]),
    BrowserAnimationsModule
  ],

  bootstrap: [AppComponent],

})

export class AppModule { }
