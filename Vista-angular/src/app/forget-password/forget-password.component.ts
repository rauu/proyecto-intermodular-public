import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Server } from '../services/server';



@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

 forget!: FormGroup;
 submitted = false;

  constructor(private formBuilder: FormBuilder, public server: Server) { }

  ngOnInit(): void {
    this.forget = this.formBuilder.group({
      user: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]

    });
  }

  forgetPassword(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.forget.invalid) {
      alert("pon user");
      return;
    } else {
      alert("mira correo");
      this.submitted = false;
    }
    this.addEvent();
    // display form values on success
  }

  addEvent(){

    this.server.toDo('forgetPassword', this.forget.value.user, this.forget.value.email).subscribe();
  }





}
