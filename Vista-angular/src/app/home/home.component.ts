import { Component, OnInit } from '@angular/core';
import { Server } from '../services/server';
import { ObservablesService } from '../services/observables.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  search = '';

  constructor(public server:Server, public observables: ObservablesService , private router: Router) { }

  ngOnInit(): void {

    this.observables.addVar(0);

    if(localStorage.getItem("username") || sessionStorage.getItem("username")){
      this.router.navigate(['/homeUser']);
    }

    this.server.read('post', '', 'all').subscribe(
      //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
      (res) => {
        this.observables.addPost(res);
      },
      (err) =>{
        //console.log(err);
      });
  }


    //FUNCION QUE GESTIONA EL ANCHO Y ALTO DE LAS IMAGENES
    resize(pic:any){

      let width = pic.naturalWidth;
      let height = pic.naturalHeight;

      while(width > 500 || height > 400){
        width = width / 1.2;
        height = height / 1.2;
      }

      pic.style.width = width + 'px';
      pic.style.heigth = height + 'px';
    }
}
