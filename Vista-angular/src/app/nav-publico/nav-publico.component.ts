import {
  Component, OnInit, ViewChild, ElementRef
} from '@angular/core';
import { ObservablesService } from '../services/observables.service';
import { Server } from '../services/server';


@Component({
  selector: 'nav-publico',
  templateUrl: './nav-publico.component.html',
  styleUrls: ['./nav-publico.component.css']
})
export class NavPublicoComponent implements OnInit {

  search_focus = false;
  tagText = '';
  userText = '';

  constructor(public observables: ObservablesService, public server: Server) {
  }

  ngOnInit(): void {}

  myFunction(): boolean {

    //let elements: NodeListOf<Element> = document.getElementsByClassName("form-inline");

    let classes: DOMTokenList = document.getElementsByClassName("form-inline")[0].classList;
    return classes.toggle("responsive");

  }

  focus_search_true(){
    this.search_focus = true;
  }
  focus_search_false(){
    this.search_focus = false;
  }

  tagSearch() {

    if (this.tagText == '') {

      this.server.read('post', '', 'all').subscribe(
        //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
        (res) => {

            this.observables.addPost(res);
        },
        (err) => {
          //console.log(err);
        });
    } else {

      this.server.toDo('tagSearch', '', this.tagText).subscribe(
        (res) => {
          this.observables.addPost(res);
        },
        (err) => {
          let array = <any>[];
          this.observables.addPost(array);
        });
    }
  }

  userSearch() {

    if (this.userText == '') {

      let array = <any>[];
      this.observables.addUser(array);

    } else {

      this.server.toDo('userSearch', '', this.userText).subscribe(
        (res) => {

          if(res == '0 resultados'){
            let array = <any>[];
            this.observables.addUser(array);
          }else{
            this.observables.addUser(res);
          }
        },
        (err) => {
          let array = <any>[];
          this.observables.addUser(array);
        });
    }
  }
}
