import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavPublicoComponent } from './nav-publico.component';

describe('NavPublicoComponent', () => {
  let component: NavPublicoComponent;
  let fixture: ComponentFixture<NavPublicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavPublicoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavPublicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
