import { Component, OnInit } from '@angular/core';
import { Server } from '../services/server';
import { Location } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../Clases/user';
import { ObservablesService } from '../services/observables.service';

// import custom validator to validate that password and confirm password fields match and email
import { MustMatch } from './must-match.validator';
import { matchEmail } from './match-email.validator';
import { AppComponent } from '../app.component';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-administrador-usuarios',
  templateUrl: './administrador-usuarios.component.html',
  styleUrls: ['./administrador-usuarios.component.css']
})
export class AdministradorUsuariosComponent implements OnInit {


  registerForm!: FormGroup;
  editUserForm!: FormGroup;
  submitted = false;
  editSubmitted = false;
  maxDate = new Date();
  userLog = new User;
  userText = '';
  userExists = true;


  /*Edit users*/
  usernameEdit = '';
  nameEdit = '';
  bioEdit = '';

  isAdminEdit = false;
  control!: FormControl;



  constructor(private method: Server, private location: Location, private formBuilder: FormBuilder, private router: Router, public observables: ObservablesService) { }


  userForm: any;

  ngOnInit(): void {

    this.observables.addVar(3);

    //LLAMADAS AJAX AL SERVIDOR Y COMPROBACIÓN DEL SESSION O LOCAL STORAGE
    //CORRESPONDIENTE AL USERNAME DEL USUARIO
    if (sessionStorage.getItem("username")) {

      this.method.read('user', sessionStorage.getItem("username"), '').subscribe(
        //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
        (res) => {
          if (sessionStorage.getItem("username") == res[0].username) {
            this.userLog = res[0];
            if (this.userLog.admin != 1) {
              this.router.navigate(['/homeUser']);
            }
          } else {
            this.router.navigate(['/login']);
          }
        },
        (err) => {
          this.router.navigate(['/login']);
        });

    } else {

      if (localStorage.getItem("username")) {

        this.method.read('user', localStorage.getItem("username"), '').subscribe(
          //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
          (res) => {
            if (localStorage.getItem("username") == res[0].username) {
              this.userLog = res[0];
              if (this.userLog.admin != 1) {
                this.router.navigate(['/homeUser']);
              }
            } else {
              this.router.navigate(['/login']);
            }
          },
          (err) => {
            this.router.navigate(['/login']);
          });

      } else {
        this.router.navigate(['/login']);
      }
    }

    //User form
    this.registerForm = this.formBuilder.group({
      Name: ['', Validators.required],
      username: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(9), Validators.pattern("^[0-9]*$")]],
      // validates date format yyyy-mm-dd
      dob: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      isAdmin: [false, Validators.required],
    }, {
      validator: MustMatch('password', 'confirmPassword'),
      emailvalidator: MustMatch('email', 'confirmEmail'),



    }
    );

    this.editUserForm = this.formBuilder.group({
      editName: [''],
      editUsername: ['', Validators.required],
      editBio: [''],
      editAdmin: [false],
    }
    );
    this.control = this.formBuilder.control({ value: 'my val', disabled: true });




    //Cargar usuarios al recargar

    this.method.read('user', '', 'all').subscribe(
      (res) => {
        this.observables.addUser(res);
      },
      (err) => {
        alert("No se han podido cargar los usuarios. El error es:\n" + err);
      }
    );

  }

  /* REGISTER USERS FUCNTIONS*/
  get f() { return this.registerForm.controls; }
  get e() { return this.editUserForm.controls; }

  onSubmit() {
    this.submitted = true;


    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    } else if(!this.userExists){
      alert("User already exits");
      $('.user-error').toast('show');
      return;
    }else {
      this.submitted = false;

    }
    this.addEvent();
    $('.toast').toast('show');


    // display form values on success
  }

  users = <any>[];
  addEvent() {

    let admin = 0;

    if (this.registerForm.value.isAdmin == true) {
      admin = 1;
    } else {
      admin = 0;
    }

    let array = <any>[this.registerForm.value.Name, this.registerForm.value.dob, this.registerForm.value.username, this.registerForm.value.phone, this.registerForm.value.email, this.registerForm.value.password, admin];
    this.method.insert("user-admin", array).subscribe(
      (res) => {
        //console.log(JSON.parse(res));
      },
      (err) => {
        //console.log(err);
      }

    );
    //

  }


  /* EDIT USER FUNCTIONS*/

  userSearch() {

    if (this.userText == '') {

      let array = <any>[];
      this.observables.addUser(array);

    } else {

      this.method.toDo('userSearch', '', this.userText).subscribe(
        (res) => {

          if (res == '0 resultados') {
            let array = <any>[];
            this.observables.addUser(array);
          } else {
            this.observables.addUser(res);
          }
        },
        (err) => {
          let array = <any>[];
          this.observables.addUser(array);
        });
    }
  }

  userData(user: any) {

    if (this.bioEdit == null) {
      this.bioEdit = '';
    }else{
      this.bioEdit = user.bio;
    }
    //this.count = user.bio.value;
    if (user.admin == 1) {
      this.isAdminEdit = true;
    } else {
      this.isAdminEdit = false;
    }

    this.editUserForm = this.formBuilder.group({
      editName: [user.name],
      editUsername: [user.username, Validators.required],
      editBio: [this.bioEdit],
      editAdmin: [ this.isAdminEdit],
    }
    );


  }

  onEditUser() {
    this.editSubmitted = true;


    // stop here if form is invalid
    if (this.editUserForm.invalid) {
      return;
    } else {
      this.editSubmitted = false;
    }
    this.UpdateUser();
    $('.admin-toast').toast('show');
  }

  UpdateUser() {
    let admin = 0;
    if (this.editUserForm.value.editAdmin == true) {
      admin = 1;
    } else {
      admin = 0;
    }

    let array = <any>["UpdateAdmin", this.editUserForm.value.editName, this.editUserForm.value.editUsername, this.editUserForm.value.editBio, admin];
    this.method.update('user', this.editUserForm.value.editUsername, array).subscribe(
      (res) => {
        //console.log(res);
      }, (err) => {
        //console.log(err);
      }
    )
  }

  deleteUser() {
    if (confirm("Are you sure to delete " + this.editUserForm.value.editUsername + "?")) {
      this.method.delete('user', this.editUserForm.value.editUsername).subscribe(
        (res) => {
          //console.log(res);
        }, (err) => {
          //console.log(err);
        }
      )
    }
    window.location.reload();
    $('.admin-delete-user').toast('show');
  }

  userTextRegister = '';
  userSearchRegister() {
    this.userText = this.registerForm.value.username;
    if (this.userText == '') {

      let array = <any>[];
      this.observables.addUser(array);

    } else {

      this.method.toDo('finalUserSearch', this.userText, '').subscribe(
        (res) => {

          if (res == '0 resultados') {
            let array = <any>[];
            this.observables.addUser(array);
            this.userExists =  true;
          } else {
            this.observables.addUser(res);
            this.userExists =  false;
          }
        },
        (err) => {
          let array = <any>[];
          this.observables.addUser(array);
        });
    }
  }
}
