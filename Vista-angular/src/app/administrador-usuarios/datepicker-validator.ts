import { ValidatorFn, AbstractControl } from '@angular/forms';

export function datePickerValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let forbidden = true;
    if (control.value) {
      const moment: Date = control.value;
      if (moment.getFullYear() > moment.getFullYear() - 14 ) {
        forbidden = false;
      }
    }
    return forbidden ? { 'invalidDOBYear': true } : null;
  };
}
