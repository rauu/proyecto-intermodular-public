import { Component, OnInit } from '@angular/core';
import { Server } from '../services/server';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import custom validator to validate that password and confirm password fields match and email
import { MustMatch } from './must-match.validator';
import { matchEmail } from './match-email.validator';
import { AppComponent } from '../app.component';
import { datePickerValidator } from './datepicker-validator';
import { Router } from '@angular/router';
import { ObservablesService } from '../services/observables.service';
declare var $: any;



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  registerForm!: FormGroup;
  submitted = false;
  maxDate = new Date();
  minDate: Date;

  userExists = true;


  constructor(public observables: ObservablesService,private method: Server, private location: Location, private formBuilder: FormBuilder, private router: Router) {
    //this.parentFun.emit();
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 10, 0, 1);
  }

  userForm: any;

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      Name: ['', Validators.required],
      username: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(9), Validators.pattern("^[0-9]*$")]],
      // validates date format yyyy-mm-dd
      dob: ['', [Validators.required, datePickerValidator()]],
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      acceptTerms: [false, Validators.required],
    }, {
      validator: MustMatch('password', 'confirmPassword'),
      emailvalidator: MustMatch('email', 'confirmEmail'),



    }
    );
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;


    // stop here if form is invalid
    if (this.registerForm.invalid) {
      $('.toast').toast('show');
      return;
    } else if(!this.userExists){
      $('.toast').toast('show');
      return;
    }else{
      this.submitted = false;
    }
    this.addEvent();
    // display form values on success
  }

  users = <any>[];
  addEvent() {

    let array = <any>[this.registerForm.value.Name, this.registerForm.value.dob, this.registerForm.value.username, this.registerForm.value.phone, this.registerForm.value.email, this.registerForm.value.password];
    this.method.insert("user", array).subscribe(
      (res)=>{
        //console.log(JSON.parse(res));
      },
      (err)=>{
        //console.log(err);
      }
    );

   this.router.navigate(['/login']);
  }


  userText = '';
  userSearch() {
    this.userText = this.registerForm.value.username;
    if (this.userText == '') {

      let array = <any>[];
      this.observables.addUser(array);

    } else {

      this.method.toDo('finalUserSearch', this.userText, '').subscribe(
        (res) => {

          if (res == '0 resultados') {
            let array = <any>[];
            this.observables.addUser(array);
            this.userExists =  true;
          } else {
            this.observables.addUser(res);
            this.userExists =  false;
          }
        },
        (err) => {
          let array = <any>[];
          this.observables.addUser(array);
        });
    }
  }
}
