import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Server } from '../services/server';
import { ObservablesService } from '../services/observables.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  idusername = '';
  idpassword = '';
  errorMessage = '';
  remember = false;

  constructor(private router: Router, public server:Server, public observables: ObservablesService) { }

  ngOnInit(): void {
    if(localStorage.getItem("username")){
      this.router.navigate(['/homeUser']);
    }
  }



  onButtonClick() {


    this.server.toDo('login', this.idusername, this.idpassword).subscribe(
      (res) => {

        if(res){
          //CREAMOS COOKIE DE SESIÓN
          if(this.remember){
            localStorage.setItem('username', this.idusername);
          }
          sessionStorage.setItem('username', this.idusername);
          //REDIRIJIMOS AL HOMEUSER
          this.router.navigate(['/homeUser']);
        }else{
          this.errorMessage = "That looks like the password you wrote is incorrect. Try again.";
        }

      },
      (err) =>{
        this.errorMessage = "That looks like the username you wrote is incorrect. Try again.";
      }
    );
  }

  setCookie(name: string) {
    document.cookie =  "username=" + name + "; Secure";
  }
}
