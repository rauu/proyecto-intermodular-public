import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appDrop]'
})
export class DropDirective {
  @HostBinding('class.fileover')
  fileOver!: boolean;
  constructor() { }


  //Dragover listner
  @HostListener('dragover', ['$event']) onDragOver(evt: { preventDefault: () => void; stopPropagation: () => void; }) {
    evt.preventDefault();
    evt.stopPropagation();
    this.fileOver = true;

    console.log("dragover");
  }

  //Dragleave listener

  @HostListener('dragleave',['$event']) public OnDragLeave(evt: { preventDefault: () => void; stopPropagation: () => void; }){
    evt.preventDefault();
    evt.stopPropagation();
    console.log("dragleave");
  }

  //Drop listener

  @HostListener('drop',['$event']) public ondrop(evt: { preventDefault: () => void; stopPropagation: () => void; dataTransfer: { files: any; }; }){
    evt.preventDefault();
    evt.stopPropagation();
    //this.fileOver = false;
    let files = evt.dataTransfer.files;
    if(files.length > 0){
      let p = document.getElementById("photo_name");
      if(files[0].type == "image/jpg" || files[0].type == "image/jpeg" || files[0].type == "image/png"){
        console.log(files[0].type);
     p!.innerHTML = "Photo Selected = " + files[0].name;
      }else{
        alert("Error las imagenes solo puedes subir en formato: jpeg, jpg o png");
        files = [];
        console.log(files);
        p!.innerHTML = "No Photo selected";
      }

    }
  }



}
