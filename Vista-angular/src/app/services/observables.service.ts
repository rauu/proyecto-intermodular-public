import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from '../Clases/user';

@Injectable({
  providedIn: 'root'
})

export class ObservablesService {

  //Variable
  public variable$ = new Subject<any>();
  public variable:any;


  //USUARIOS
  private users$ = new Subject<any>();
  private users = <any>[];

  //POSTS
  private posts$ = new Subject<any>();
  private posts = <any>[];

  //W_POSTS
  private w_posts$ = new Subject<any>();
  public w_posts = <any>[];

  //LIKES
  private likes$ = new Subject<any>();
  public likes = 0;

  //COMMENTS
  private comments$ = new Subject<any>();
  public comments = <any>[];

  //REPLIES
  public replies$ = new Subject<any>();
  public replies = <any>[];

  constructor() {}


      //FUNCIONES USERS
      addUser(user: any) {
        this.users = user;
        this.users$.next(this.users);
      }
      getUsers$(): Observable<any> {
        return this.users$.asObservable();
      }
      getUsers():any {
        return this.users;
      }

      //FUNCIONES POSTS
      addPost(post: any[]) {
          this.posts = post.reverse();
          this.posts$.next(this.posts);
      }
      getPosts$(): Observable<any> {
        return this.posts$.asObservable();
      }
      getPosts():any {
        return this.posts;
      }

      //FUNCIONES W_POSTS
      addW_post(w_post: any) {
        this.w_posts = w_post;
        this.w_posts$.next(this.w_posts);
      }
      getW_posts$(): Observable<any> {
        return this.w_posts$.asObservable();
      }
      getW_posts():any {
        return this.w_posts;
      }

      //FUNCIONES Comments
      addLikes(likes: any) {
        this.likes = likes;
        this.likes$.next(this.likes);
      }
      getLikes$(): Observable<any> {
        return this.likes$.asObservable();
      }
      getLikes():any {
        return this.likes;
      }

      //FUNCIONES Comments
      addComments(comments: any[]) {
        try {
          this.comments = comments.reverse();
          this.comments$.next(this.comments);
        } catch (error) {
        }
      }
      getComments$(): Observable<any> {
        return this.comments$.asObservable();
      }
      getComments():any {
        return this.comments;
      }

      //FUNCIONES Comments
      addReplies(replies: any[]) {
        try {
          this.replies = replies;
          this.replies$.next(this.replies);
        } catch (error) {
        }
      }
      getReplies$(): Observable<any> {
        return this.replies$.asObservable();
      }
      getReplies():any {
        return this.replies;
      }

      //Funciones var
      addVar(variable: any) {
        this.variable = variable;
        this.variable$.next(this.variable);
      }
      getVar$(): Observable<any> {
        return this.variable$.asObservable();
      }
      getVar(): Observable<any> {
        return this.variable;
      }

}
