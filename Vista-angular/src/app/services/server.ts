import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ObservablesService } from '../services/observables.service';
import { empty } from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class Server {

  default = empty();

  constructor(private http:HttpClient, public observables: ObservablesService) {
   }

    postServer(formData: FormData): Observable<any>{
      return this.http.post<any>
      ('https://picult.com/picult/Control/Server/Controller/insert.php', formData)
    }

    getServer(object: string, username: string, id: string): Observable<any>{
      return this.http.get<any>
      ('https://picult.com/picult/Control/Server/Controller/read.php?object=' + object + '&username=' + username + '&id=' + id)
    }

    putServer(object: Object): Observable<any>{
      return this.http.put<any>
      ('https://picult.com/picult/Control/Server/Controller/update.php', object)
    }

    deleteServer(object: string, id: string): Observable<any>{
      return this.http.delete<any>
      ('https://picult.com/picult/Control/Server/Controller/delete.php?object=' + object + '&id=' + id);
    }

    doServer(formData: FormData): Observable<any>{
      return this.http.post<any>('https://picult.com/picult/Control/Server/Controller/do.php', formData);
    }


  //FUNCIÓN PARA MANDAR DATOS AL SERVIDOR, LOS DATOS A INSERTAR
  //DEBERÁN IR EN ORDEN SEGÚN LA VISTA
  insert(object: string, datos: []): Observable<any>{
    let formData: FormData = new FormData();
    formData.append('object', JSON.stringify(object));
    formData.append('datos', JSON.stringify(datos));

    return this.postServer(formData);
  }

  //FUNCIÓN PARA RECOGER DATOS DEL SERVIDOR, DEBERÁS INTRODUCIR
  //EL OBJETO QUE QUIERAS RECIBIR(user, post, w_post) Y SU ID O USERNAME.
  read(object: string, username: any, id: any): Observable<any> {

    return this.getServer(object, username, id);

  }

  //FUNCIÓN PARA ACTUALIZAR DATOS DEL SERVIDOR, LOS DATOS A INSERTAR
  //DEBERÁN IR EN ORDEN SEGÚN LA VISTA Y DEBERÁN ESPECIFICARSE TODOS.
  update(object: string, id: string, datos: []): Observable<any> {
    let objeto = <any>{};
    objeto.object = object;
    objeto.id = id;
    objeto.datos = datos;

    let json = JSON.stringify(objeto);

    return this.putServer(json);
  }

  //FUNCIÓN PARA ELIMINAR DATOS DEL SERVIDOR, SE DEBERÁ
  //ESPECIFICAR EL OBJETO A ELIMINAR Y SU ID
  delete(object: string, id: any): Observable<any> {

    return this.deleteServer(object,id);
  }

  //FUNCIÓN PARA ESPECIFICAR LAS FUNCIONES QUE TIENE QUE REALIZAR EL SERVIDOR,
  //ESPECIFICAR EN EL PARAMETRO OBJECT LA FUNCIÓN A REALIZAR Y EN EL ID SI ES
  //REQUERIDO ALGÚN PARAMETRO MÁS, SI NO PONER COMILLAS VACÍAS
  toDo(object: string, username: string , id: string):Observable<any>{
    let formData: FormData = new FormData();
    formData.append('object', JSON.stringify(object));
    formData.append('username', JSON.stringify(username));
    formData.append('id', JSON.stringify(id));


    return this.doServer(formData);

    }
}

