import { Component, OnInit } from '@angular/core';
import { Server } from '../services/server';
import { Location } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

// import custom validator to validate that password and confirm password fields match and email
import { MustMatch } from './must-match.validator';
import { matchEmail } from './match-email.validator';
import { AppComponent } from '../app.component';
import { Router, ActivatedRoute } from '@angular/router';
import { ObservablesService } from '../services/observables.service';
import { User } from '../Clases/user';
declare var $: any;

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {

  editUserPassword!: FormGroup;
  editUserData!: FormGroup;
  submitted = false;
  submitted_password = false;
  maxDate = new Date();
  minDate: Date;

  control!: FormControl;

  constructor(public observables: ObservablesService, private method: Server, private location: Location, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) {
    //this.parentFun.emit();
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 10, 0, 1);
  }

  userForm: any;
  userLog = new User();

  ngOnInit(): void {
    this.observables.addVar(3);

    this.editUserData = this.formBuilder.group({
      Name: [this.userLog.name, Validators.required],
      username: [this.userLog.username, Validators.required],
      phone: [this.userLog.phone, [Validators.required, Validators.minLength(9), Validators.pattern("^[0-9]*$")]],
      // validates date format yyyy-mm-dd
      dob: [this.userLog.birthDate, [Validators.required]],
      email: [this.userLog.e_mail, [Validators.required, Validators.email]],
      confirmEmail: [this.userLog.e_mail, [Validators.required, Validators.email]],
    });

    //Recoger los datos del usuario
    if (sessionStorage.getItem("username")) {

      this.method.read('user', sessionStorage.getItem("username"), '').subscribe(
        //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
        (res) => {
          if (sessionStorage.getItem("username") == res[0].username) {
            this.userLog = res[0];
            this.editUserData = this.formBuilder.group({
              Name: [this.userLog.name, Validators.required],
              username: [this.userLog.username, Validators.required],
              phone: [this.userLog.phone, [Validators.required, Validators.minLength(9), Validators.pattern("^[0-9]*$")]],
              // validates date format yyyy-mm-dd
              dob: [this.userLog.birthDate, [Validators.required]],
              email: [this.userLog.e_mail, [Validators.required, Validators.email]],
              confirmEmail: [this.userLog.e_mail, [Validators.required, Validators.email]],
            });
          } else {
            this.router.navigate(['/login']);
          }
        },
        (err) => {
          this.router.navigate(['/login']);
        });

    } else {

      if (localStorage.getItem("username")) {

        this.method.read('user', localStorage.getItem("username"), '').subscribe(
          //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
          (res) => {
            if (localStorage.getItem("username") == res[0].username) {
              this.userLog = res[0];
            } else {
              this.router.navigate(['/login']);
            }
          },
          (err) => {
            this.router.navigate(['/login']);
          });

      } else {
        this.router.navigate(['/login']);
      }
    }






    this.editUserPassword = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmPassword'),
    });
    this.control = this.formBuilder.control({ value: 'my val', disabled: true });
  }


  get user() { return this.editUserData.controls; }
  get password() { return this.editUserPassword.controls; }

  users = <any>[];
  addEvent() {
    //$name, $birthDate, $username, $phone, $email
    let array = <any>["UpdateSettings", this.editUserData.value.Name, this.editUserData.value.dob, this.editUserData.value.username, this.editUserData.value.phone, this.editUserData.value.email];
    this.method.update("user", this.userLog.username, array).subscribe(
      (res) => {
        //console.log(JSON.parse(res));

      },
      (err) => {
        //console.log(err);
      }
    );
  }

  editUserDataFunction() {
    this.submitted = true;


    // stop here if form is invalid
    if (this.editUserData.invalid) {
      return;
    } else {
      this.submitted = false;
    }
    this.addEvent();
    $('.edit-toast').toast('show');
    //window.location.reload();
  }
  editUserPasswordFunction() {
    this.submitted_password = true;


    // stop here if form is invalid
    if (this.editUserPassword.invalid) {
      return;
    } else {
      this.submitted_password = false;
    }
    this.editPassword();
    $('.password-toast').toast('show');

  }

  editPassword(){
    let array = <any>["UpdatePassword", this.userLog.username, this.editUserPassword.value.password, this.userLog.password];
    this.method.update("user", this.userLog.username, array).subscribe(
      (res) => {
        //console.log(JSON.parse(res));

      },
      (err) => {
        //console.log(err);
      }
    );
  }



}
