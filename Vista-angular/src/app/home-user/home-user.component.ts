import { Component, DoCheck, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Server } from '../services/server';
import { ObservablesService } from '../services/observables.service';
import { ActivatedRoute, Router} from '@angular/router';
import { User } from '../Clases/user';

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.component.html',
  styleUrls: ['./home-user.component.css']
})

export class HomeUserComponent implements OnInit {

  userLog = new User;
  search = '';
  variable = false;
  idpost = -1;
  display = {
    'display': 'none'
  };

  constructor(public server:Server, public observables: ObservablesService , private route: ActivatedRoute, private router: Router, private elementRef: ElementRef) {
  }

  ngOnInit(){

    this.observables.addVar(0);

    //LLAMADAS AJAX AL SERVIDOR Y COMPROBACIÓN DEL SESSION O LOCAL STORAGE
    //CORRESPONDIENTE AL USERNAME DEL USUARIO
    if(sessionStorage.getItem("username")){

      this.server.read('user', sessionStorage.getItem("username") , '').subscribe(
        //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
        (res) => {
          if(sessionStorage.getItem("username") == res[0].username){
            this.userLog = res[0];
          }else{
            this.router.navigate(['/login']);
          }
        },
        (err) =>{
            this.router.navigate(['/login']);
        });

    }else{

      if(localStorage.getItem("username")){

        this.server.read('user', localStorage.getItem("username") , '').subscribe(
          //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
          (res) => {
            if(localStorage.getItem("username") == res[0].username){
              this.userLog = res[0];
            }else{
              this.router.navigate(['/login']);
            }
          },
          (err) =>{
            this.router.navigate(['/login']);
          });

      }else{
        this.router.navigate(['/login']);
      }
    }

    this.server.read('post', '', 'all').subscribe(
      //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
      (res) => {
        this.observables.addPost(res);
      },
      (err) =>{
        //console.log(err);
      });

      this.server.read('user', '', 'all').subscribe(
        //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
        (res) => {
          this.observables.addUser(res);
        },
        (err) =>{
          //console.log(err);
        });
  }


  //FUNCION QUE GESTIONA EL ANCHO Y ALTO DE LAS IMAGENES
  resize(pic:any){

    let width = pic.naturalWidth;
    let height = pic.naturalHeight;

    while(width > 500 || height > 400){
      width = width / 1.2;
      height = height / 1.2;
    }

    pic.style.width = width + 'px';
    pic.style.heigth = height + 'px';

  }


  //ASIGNAR UN ID A LA VARIABLE IDPOST
  postId(id: any){
    this.idpost = id;
    this.display['display'] = 'flex';
  }
}
