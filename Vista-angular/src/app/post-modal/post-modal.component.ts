import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Server } from '../services/server';
import { ObservablesService } from '../services/observables.service';
import { Post } from '../Clases/post';

@Component({
  selector: 'app-post-modal',
  templateUrl: './post-modal.component.html',
  styleUrls: ['./post-modal.component.css']
})
export class PostModalComponent implements OnInit {

  post = new Post;
  profile_image: any;
  liked: any;
  array: any;
  @Input() id: any;
  @Output() idOut = new EventEmitter<string>();

  constructor(public server: Server, public observables: ObservablesService) { }

  ngOnInit(): void {
    this.server.read('post', '', this.id).subscribe(
      (res) => {
        this.post = res[0];

        this.server.read('user', this.post.username, '').subscribe(
          (res) => {
            this.profile_image = res[0].profile_image;

            this.server.read('value', sessionStorage.getItem("username") || localStorage.getItem("username"), this.id).subscribe(
              (res) => {
                this.liked = res;
              });
          },
          (err) => {
            console.log(err);
          }
        );

      },
      (err) => {
        console.log(err);
      }
    )
  }

  emitIdPost(value: any) {
    this.idOut.emit(value);
  }

  //FUNCION QUE GESTIONA EL ANCHO Y ALTO DE LAS IMAGENES
  resize(pic: any) {

    let width = pic.naturalWidth;
    let height = pic.naturalHeight;

    while (width > 650 || height > 450) {
      width = width / 1.2;
      height = height / 1.2;
    }

    pic.style.width = width + 'px';
    pic.style.heigth = height + 'px';

  }


  like() {

    this.array = [this.id, sessionStorage.getItem("username") || localStorage.getItem("username"), 1];
    this.server.insert('value', this.array).subscribe();
    this.server.read('post', '', this.id).subscribe(
      (res) => {
        res = res[0];
        this.array = [res.id, res.username, res.fecha, res.dateApproved, res.location, res.descripcion, res.imagen, (parseInt(res.valMedia) + 1)];
        this.server.update('post', this.id, this.array).subscribe();
      }
    );
    this.liked = '';
  }

  dislike() {

    this.array = sessionStorage.getItem("username") || localStorage.getItem("username") + '-' + this.id;
    this.server.delete('value', this.array).subscribe();
    this.server.read('post', '', this.id).subscribe(
      (res) => {
        res = res[0];
        this.array = [res.id, res.username, res.fecha, res.dateApproved, res.location, res.descripcion, res.imagen, (parseInt(res.valMedia) - 1)];
        this.server.update('post', this.id, this.array).subscribe();
      }
    );
    this.liked = '0 resultados';
  }
}
