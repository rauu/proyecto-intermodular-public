import { Component, OnInit } from '@angular/core';
import { Server } from '../services/server';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../Clases/user';
import { Wpost } from '../Clases/Wpost';
import { ObservablesService } from '../services/observables.service';
import { SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-waiting-posts',
  templateUrl: './waiting-posts.component.html',
  styleUrls: ['./waiting-posts.component.css'],
})
export class WaitingPostsComponent implements OnInit {
  userLog = new User();
  nuevaImagen = <any>[];
  wpPreparado = <any>[];
  public imagen :string = "";
  public contador : number = 0;

  constructor(public server: Server, public observables: ObservablesService, private router: Router)
  {
    this.wpPreparado;
    this.nuevaImagen;
  }

  ngOnInit(): void {

    this.observables.addVar(3);

    //LLAMADAS AJAX AL SERVIDOR Y COMPROBACIÓN DEL SESSION O LOCAL STORAGE
    //CORRESPONDIENTE AL USERNAME DEL USUARIO
    if (sessionStorage.getItem('username')) {
      this.server.read('user', sessionStorage.getItem('username'), '').subscribe(
          //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
          (res) => {
            if (sessionStorage.getItem('username') == res[0].username) {
              this.userLog = res[0];
              if (this.userLog.admin != 1) {
                this.router.navigate(['/homeUser']);
              }
            } else {
              this.router.navigate(['/login']);
            }
          },
          (err) => {
            this.router.navigate(['/login']);
          }
        );
    } else {
      if (localStorage.getItem('username')) {
        this.server
          .read('user', localStorage.getItem('username'), '')
          .subscribe(
            //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
            (res) => {
              if (localStorage.getItem('username') == res[0].username) {
                this.userLog = res[0];
                if (this.userLog.admin != 1) {
                  this.router.navigate(['/homeUser']);
                }
              } else {
                this.router.navigate(['/login']);
              }
            },
            (err) => {
              this.router.navigate(['/login']);
            }
          );
      } else {
        this.router.navigate(['/login']);
      }
    }

    this.server.read('w_posts', '', 'all').subscribe(
      //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
      (res) => {
        this.observables.addW_post(res);
      },
      (err) => {
        //console.log(err);
      }
    );
  }

  imagenUser(username : string) {
    this.server.read('user', username, '').subscribe(
      (res) => {
        this.imagen = res[0].profile_image;
        let elemento = document.getElementsByClassName("imagenL")[this.contador];
        elemento?.setAttribute("src",this.imagen);
        this.contador++;

      },
      (err) => {
        //console.log(err);
      }
    );

  }

  //Post a Borar
  declineWPost(wpBorrar: any) {
    this.contador = 0;
    this.server.delete('w_post',wpBorrar.id).subscribe(
      (res) =>{
        //console.log(res);
        //console.log("1");
      },(err) =>{
        //console.log("2");
       // console.log(err);
        this.server.read('w_posts', '', 'all').subscribe(
          //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
          (res1) => {
            //console.log(res1);
            this.observables.addW_post(res1);
          },
          (err) => {
            //console.log(err);
          }
        );
      }
    )



  }

  //Post a aceptar
  acceptWPost(wpInsertar: any) {
    this.contador = 0;
    //Metemos todos los datos necesarios para realizar la query en un array en el mismo orden que en el modelo
    this.wpPreparado.push(wpInsertar.username, wpInsertar.location, wpInsertar.imageWP, wpInsertar.fecha, wpInsertar.descripcion, wpInsertar.tagNames);
    //insertamos ese array en el controler que procedera a mandarlo al modelo
    this.server.insert('w_post', this.wpPreparado).subscribe(
      (res) => {
        //console.log(res);
      },
      (err) => {
        this.server.read('w_posts', '', 'all').subscribe(
          (res1) => {
            this.observables.addW_post(res1);
            this.wpPreparado = <any>[];

          },(err) =>{
            //console.log(err);
          }
        )
      }
    );






  }

  //FUNCION QUE GESTIONA EL ANCHO Y ALTO DE LAS IMAGENES
  resize(pic: any) {
    let width = pic.naturalWidth;
    let height = pic.naturalHeight;

    while (width > 500 || height > 400) {
      width = width / 1.2;
      height = height / 1.2;
    }

    pic.style.width = width + 'px';
    pic.style.heigth = height + 'px';
  }



}
