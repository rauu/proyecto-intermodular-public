import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingPostsComponent } from './waiting-posts.component';

describe('WaitingPostsComponent', () => {
  let component: WaitingPostsComponent;
  let fixture: ComponentFixture<WaitingPostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WaitingPostsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
