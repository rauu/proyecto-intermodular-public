import { Component } from '@angular/core';
import { Server } from './services/server';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent{
  title = 'Vista-Angular';

  constructor(public server: Server){}
}
