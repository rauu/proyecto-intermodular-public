import { Component, OnInit } from '@angular/core';
import { Server } from '../services/server';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../Clases/user';
import { ObservablesService } from '../services/observables.service';
import { SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Post } from '../Clases/post';
import {Directive, Input, ViewChild} from '@angular/core';
import { ElementRef } from '@angular/core';
import { datePickerValidator } from '../register/datepicker-validator';

declare var $: any;

@Component({
  selector: 'app-info-user',
  templateUrl: './info-user.component.html',
  styleUrls: ['./info-user.component.css'],
})

export class InfoUserComponent implements OnInit {
  userLog = new User();
  userSearch = new User();
  posts = <any>[];
  display = {
    'display':'none'
  };
  likes = 0;
  totalPosts = 0;
  datas!: FormGroup;
  submitted = false;
  public sessionStorage = sessionStorage.getItem('username');
  public localStorage = localStorage.getItem('username');
  public numPost: number = 0;
  userImagen = "";
  Comprobar = false;
  constructor(
    public server: Server,
    public observables: ObservablesService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.numPost;
    this.Comprobar;
  }

  ngOnInit(): void {

    if(sessionStorage.getItem("username")){

      this.server.read('user', sessionStorage.getItem("username") , '').subscribe(
        //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
        (res) => {
          if(sessionStorage.getItem("username") == res[0].username){
            this.userLog = res[0];
          }else{
            this.router.navigate(['/login']);
          }
        },
        (err) =>{
            this.router.navigate(['/login']);
        });

    }else{

      if(localStorage.getItem("username")){

        this.server.read('user', localStorage.getItem("username") , '').subscribe(
          //SI LA RESPUESTA DEL SERVIDOR ES CORRECTA CAMBIAMOS EL VALOR DEL OBSERVABLE
          (res) => {
            if(localStorage.getItem("username") == res[0].username){
              this.userLog = res[0];
            }else{
              this.router.navigate(['/login']);
            }
          },
          (err) =>{
            this.router.navigate(['/login']);
          });

      }else{

      }
    }

    this.datas = this.formBuilder.group({
      Image: [this.userLog.profile_image],
      Name: [this.userLog.name],
      Bio: [this.userLog.bio],
      Location: [this.userLog.location],
    });

    //Función para recoger datos del usuario buscado
    this.server
      .read('user', this.route.snapshot.paramMap.get('username'), '')
      .subscribe(
        (res) => {

          this.observables.addVar(3);
          this.userSearch = res[0];



          if (res[0] == '0') {
            this.router.navigate(['/error/404']);
          }
        },
        (err) => {
          this.router.navigate(['/error/404']);
        }
      );

    this.server
      .read('post', this.route.snapshot.paramMap.get('username'), '')
      .subscribe(
        (res) => {
          if (res != '0 resultados') {
            this.posts = res.reverse();

            for (let index = 0; index < this.posts.length; index++) {
              const element = this.posts[index];
              this.totalPosts = this.totalPosts + 1;
              this.likes = this.likes + parseInt(element.valMedia);
            }

          }
        },
        (err) => {
          //console.log(err);
        }
      );

      this.server.read('post', this.userSearch.username, 'like').subscribe(
        (res) =>{
          //console.log(this.userSearch.username);


        },(err) =>{
          //console.log(err);
        }
      )
  }

  comprobarLog() {
    if (!sessionStorage.getItem("username") && !localStorage.getItem("username")) {
      return false;
    } else {
      return true;
    }
  }


  get dataReturn() {
    return this.datas.controls;
  }

  addDatas(file: any) {
    if (this.datas.invalid) {
      return;
    } else {
      $('.vedit').toast('show');
      this.addNewDatas(file);
      this.submitted = false;
    }
  }

  addNewDatas(file: any) {

    if (this.datas.value.Image != '') {
      let split = file.files[0].type;
      let  extensions = split.split('/', 2);

      const reader: FileReader = new FileReader();
      reader.readAsDataURL(file.files[0]);
      reader.addEventListener('loadend', (e) => {
      let  blopPhoto= reader.result as string;


        let array = <any>[
          "UserWP",
          this.datas.value.Name,
          this.datas.value.Bio,
          this.datas.value.Location,
          blopPhoto,
          extensions[1],
          this.userLog.username,
        ];
        this.server.update("user",this.userLog.username,array).subscribe();
      });

    }else{
      let blopPhoto = this.userLog.profile_image;

      let array = <any>[
        "UserNP",
        this.datas.value.Name,
        this.datas.value.Bio,
        this.datas.value.Location,
        blopPhoto,
        this.userLog.username,

      ];
      this.server.update("user",this.userLog.username,array).subscribe();
    }

  }
  insertDatas(){
    this.datas = this.formBuilder.group({
      Image: [''],
      Name: [this.userLog.name],
      Bio: [this.userLog.bio],
      Location: [this.userLog.location],
    });
  }

  resize(imagen: any) {
    let width = imagen.naturalWidth;
    let height = imagen.naturalHeight;

    while (width > 500 || height > 400) {
      width = width / 1.2;
      height = height / 1.2;
    }

    imagen.style.width = width + 'px';
    imagen.style.heigth = height + 'px';
  }

  cambiarImagen(imagen:any)
  {

    if(this.Comprobar == true)
    {
      const reader: FileReader = new FileReader();
      reader.readAsDataURL(imagen.files[0]);
      reader.addEventListener('loadend', e => {
      let file: any = reader.result as string;
      this.Comprobar = false;
      document.getElementById("imgs")?.setAttribute("src",file);
    });

    }else{
      $('.eedit').toast('show');
    }

  }

  cambiarValor()
  {
    this.Comprobar = true;
  }



}
