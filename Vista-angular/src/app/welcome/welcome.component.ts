import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'],
  //Obliga que aplique el CSS del componente
  encapsulation: ViewEncapsulation.None,
})
export class WelcomeComponent implements OnInit {

  constructor(private location: Location) {
    //this.pageChange();
    //this.getCookie("welcome");
    //this.setCookie();
    this.createCokkie();
    this.backgroundChange();

  }


  ngOnInit(): void {
  }

  //Funcion que devuelve una ruta del imagen a la variable img y esa variable de recoge en HTML
  img = "url('assets/welcome1.png')";
  backgroundChange() {

      let v = Math.floor(Math.random() * Math.floor(7));
      //this.setCookie();
      this.pageChange();
      switch (v) {
        case 1:
          this.img = "url('assets/welcome2.jpeg')";
          return this.img;
          break;
        case 2:
          this.img = "url('assets/welcome3.jpg')";
          return this.img;
          break;
        case 3:
          this.img = "url('assets/welcome4.jpg')";
          return this.img;
          break;
        case 4:
          this.img = "url('assets/welcome5.jpg')";
          return this.img;
          break;
        case 5:
          this.img = "url('assets/welcome6.jpg')";
          return this.img;
          break;
        case 6:
          this.img = "url('assets/welcome7.jpg')";
          return this.img;
          break;
        default:
          this.img = "url('assets/welcome1.png')";
          return this.img;

    }
  }

  //Funciton para camibiar de pagina automaticamente en 1.5 seg
  pageChange() {
    setTimeout(() => {
      this.location.replaceState('/home');
      window.location.reload();
    }, 4000);

  }

  //funcion para crear una cookie de session
  setCookie(name: string) {
    document.cookie =  name + "= welcome;"
  }

  //Funcion para recoger una cookie cualquiera
  getCookie(name: string) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ')
        c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0)
        return (c.substring(nameEQ.length, c.length));
    }
    return null;
  }

  //Funcion para crear cokkie de welcome e la pag principal
  createCokkie(){
    if (this.getCookie("welcome") == "welcome") {
      this.location.replaceState('/home');
      window.location.reload();
    }else{
      this.setCookie("welcome");
      this.pageChange();
    }
  }
}
