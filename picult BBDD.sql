-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-02-2021 a las 01:24:04
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `picult`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `comment` varchar(160) DEFAULT NULL,
  `date` datetime DEFAULT current_timestamp(),
  `id_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `description`
--

CREATE TABLE `description` (
  `id_post` int(11) NOT NULL,
  `tagName_tag` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  `dateApproved` date NOT NULL DEFAULT current_timestamp(),
  `location` varchar(50) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `imagen` varchar(300) NOT NULL,
  `valMedia` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `tagName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `username` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `phone` int(11) NOT NULL,
  `e_mail` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `profile_image` varchar(300) DEFAULT 'http://localhost/picult/Control/Server/images/users/userDefault.jpeg' COMMENT 'iVBORw0KGgoAAAANSUhEUgAABQAAAAUACAYAAAAY5P/3AAC2oElEQVR42uz9Z5cc15Uu6r4RkZFZVfDeEKA3ct29z73j/v8/cM89e3e3JHoLQ3hbVZmREbHuhypQlBokARCmKut5xoBEgBIhLkkxY74x11rVlWvXSwAAAACAlVRbAgAAAABYXQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhhAkAAAAAAWGECQAAAAABYYQJAAAAAAFhh',
  `admin` tinyint(1) DEFAULT 0,
  `valMedia` float DEFAULT 0,
  `birthDate` date NOT NULL,
  `bio` varchar(160) DEFAULT NULL,
  `numpost` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `val`
--

CREATE TABLE `val` (
  `username` varchar(25) CHARACTER SET utf8 NOT NULL,
  `id_post` int(11) NOT NULL,
  `valoration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `waiting_post`
--

CREATE TABLE `waiting_post` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `location` varchar(50) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `imageWP` varchar(300) NOT NULL,
  `tagNames` varchar(500) DEFAULT NULL,
  `fecha` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_comments_USERS` (`username`);

--
-- Indices de la tabla `description`
--
ALTER TABLE `description`
  ADD PRIMARY KEY (`id_post`,`tagName_tag`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_POST_USERS` (`username`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tagName`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Indices de la tabla `val`
--
ALTER TABLE `val`
  ADD PRIMARY KEY (`username`,`id_post`);

--
-- Indices de la tabla `waiting_post`
--
ALTER TABLE `waiting_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_USER_WAITING` (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT de la tabla `waiting_post`
--
ALTER TABLE `waiting_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `FK_comments_USERS` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Filtros para la tabla `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `FK_POST_USERS` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Filtros para la tabla `waiting_post`
--
ALTER TABLE `waiting_post`
  ADD CONSTRAINT `FK_USER_WAITING` FOREIGN KEY (`username`) REFERENCES `users` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
