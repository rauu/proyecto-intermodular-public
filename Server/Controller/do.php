<?php
header("Access-Control-Allow-Origin: http://localhost:4200");

$id = json_decode($_POST['id']);
$username = json_decode($_POST['username']);


switch (json_decode($_POST['object'])) {

    case 'login':

        include_once('../Model/Users.php');
        $controller = new Users();

        echo json_encode($controller->login($username, $id));

        break;

    case 'tagSearch':

        include_once('../Model/Description.php');
        include_once('../Model/Posts.php');

        $controller = new Description();

        if ($controller->returnDescriptionTagName($id) != '0 resultados') {

            $idPosts = $controller->returnDescriptionTagName($id);
            $controller = new Posts();
            $posts = [];

            foreach (json_decode($idPosts) as $idPost) {
                $post = json_decode($controller->returnPost($idPost->id_post))[0];
                array_push($posts, $post);
            }

            echo json_encode($posts);
        } else {
            echo json_encode('0 resultados');
        }

        break;


    case 'userSearch':

        include_once('../Model/Users.php');
        $controller = new Users();

        echo $controller->likeUser($id);

        break;



    case 'finalUserSearch':
        include_once('../Model/Users.php');
        $controller = new Users();

        echo $controller->likeFinalUser($username);

        break;


    case 'profileImage':

        include_once('../Model/Users.php');
        $controller = new Users();
        var_dump($username);
        echo $controller->returnProfileImage($username);

        break;

    case 'forgetPassword':
        include_once('../Model/Users.php');
        $controller = new Users();

        echo $controller->forgetPassword($username, $id);
        break;
    default:

        break;
}
