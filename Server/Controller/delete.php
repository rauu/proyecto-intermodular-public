<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Methods: DELETE");

if ($_SERVER['REQUEST_METHOD'] == "DELETE") {

    $object = $_GET['object'];
    $id = $_GET['id'];
    
    switch ($object) {

        case 'user':

            include_once('../Model/Users.php');
            $controller = new Users();
            echo json_encode($controller->deleteUser($id));

            break;

        case 'post':

            include_once('../Model/Posts.php');

            $controller = new Posts();
            echo ($controller->deletePost($id));

            break;

        case 'w_post':

            include_once('../Model/waitingPosts.php');
            
            $controller = new WaitingPosts();
            echo ($controller->deleteWPost($id));

            break;

        case 'value':

            include_once('../Model/value.php');
            $explode = explode('-', $id);

            $controller = new Value();
            echo ($controller->deleteValue($explode[1], $explode[0]));
    
            break;

        default:

            break;
    }
}
?>
