<?php
header("Access-Control-Allow-Origin: http://localhost:4200");

$username = $_GET['username'];
$id = $_GET['id'];

switch ($_GET['object']) {

    case 'user':

        include_once('../Model/Users.php');
        $controller = new Users();

        echo $controller->returnUser($username);

        break;

        
    case 'post':

        include_once('../Model/Posts.php');
        $user = new Posts();

        include_once('./Model/WaitingPosts.php');
        $user = new WaitingPosts();
        
        if($id == 'all'){

            echo $controller->returnAllPost();
        }
        elseif($id != ''){

            echo $controller->returnPost($id);
        }
        elseif($username != ''){

            echo $controller->returnAllPostForUser($username);
        }

        break;


    case 'w_post':

        include_once('../Model/WaitingPosts.php');
        $user = new WaitingPosts();
        
        if($id == 'all'){

            echo $controller->returnWPost();
        }
        elseif($id != ''){

            echo $controller->returnWPostsId($id);
        }
        elseif($username != ''){

            echo $controller->returnWPostsUser($username);
        }

        break;

    default:

        break;
}
