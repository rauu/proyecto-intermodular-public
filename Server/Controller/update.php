<?php
header("Access-Control-Allow-Origin: http://localhost:4200");
header("Access-Control-Allow-Methods: PUT");

if ($_SERVER['REQUEST_METHOD'] == "PUT") {

    $putBody = file_get_contents("php://input");

    $putBody = json_decode($putBody);

    $object = $putBody->object;
    $id = $putBody->id;
    $datos = $putBody->datos;

    switch ($object) {

        case 'user':

            include_once('../Model/Users.php');
            $controller = new Users();
            echo "commit";
           // $datos = json_decode($datos);

            if ($datos[0] == "UpdateAdmin") {
                echo json_encode($controller->UpdateAdmin($datos[1], $datos[2], $datos[3], $datos[4]));
            } else if ($datos[0] == "UserWP") {
                include_once('../Model/Posts.php');
                $post = new Posts();
                $length = $post->generateRandomString();
                $nombreImagen = '/var/www/picult/proyecto-intermodular/Server/images/users/' . $length .".". $datos[5];


                echo json_encode($controller->updateEditProfile($datos[1], $datos[2], $datos[3],$datos[4], $nombreImagen, $datos[6]));

            } else if ($datos[0] == "UserNP") {
                include_once('../Model/Posts.php');
                $post = new Posts();
                
                echo json_encode($controller->updateEditProfile($datos[1], $datos[2],$datos[3],$datos[0], $datos[4], $datos[5]));

            }else if($datos[0] == "UpdateAdmin"){
                echo json_encode($controller->UpdateAdmin($datos[1],$datos[2],$datos[3],$datos[4]));
            }else if($datos[0] == "UpdateSettings"){
                //hay que poner else if para mas consultas;
                    //$name, $birthDate, $username, $phone, $email
                echo json_encode($controller->updateSettings($datos[1],$datos[2],$datos[3],$datos[4], $datos[5]));
            }else if($datos[0] == "UpdatePassword"){
                //hay que poner else if para mas consultas;
                    //$name, $birthDate, $username, $phone, $email
                echo json_encode($controller->changePassword($datos[1],$datos[2],$datos[3]));
            }

            break;

        case 'post':
            
            include_once('../Model/Posts.php');
            $controller = new Posts();

            echo json_encode($controller->updatePost($datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5], $datos[6], $datos[7]));

            break;

        case 'w_post':

            include_once('../Model/waitingPosts.php');
            $controller = new WaitingPosts();

            echo json_encode(array($user));

            break;

        default:

            break;
    }
}
