<?php
header( 'Access-Control-Allow-Origin: http://localhost:4200' );

$datos = json_decode( $_POST['datos'] );

switch ( json_decode( $_POST['object'] ) ) {

    case 'user':

    include_once( '../Model/Users.php' );
    $controller = new Users();
    
    //createUsers( $name, $birthDate, $username, $phone, $email, $pass )
    echo json_encode( $controller->createUsers( $datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5] ) );

    break;

    case 'user-admin':

    include_once( '../Model/Users.php' );
    $controller = new Users();
    
    //createUsers( $name, $birthDate, $username, $phone, $email, $pass )
    echo json_encode( $controller->createUsersAdmin( $datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5], $datos[6] ) );

    break;

    case 'post':

    include_once( '../Model/Posts.php' );
    $controller = new Posts();
    $length = $controller->generateRandomString();
    echo "<br>";
    
    //createPost( $username, $location, $image, $date, $description, $tag_Name )
    //imgage(base64, extension)
    //datos($username, $location, $image, $id, $date, $description, base64, extension)
    echo json_decode( $controller->createPost( $datos[0], $datos[1], "https://picult.com/picult/Control/Server/images/waitingPosts/".$length.".".$datos[7], $datos[4], $datos[5] ) );
    echo json_encode( $controller->uploadPost($length, $datos[6], $datos[7]));
    break;

    case 'w_post':

        include_once('../Model/WaitingPosts.php');
        $controller = new WaitingPosts();

        
        echo json_encode($controller->acceptWPost($datos[0], $datos[1], $datos[2], $datos[3], $datos[4], $datos[5]));

    break;

    case 'value':

        include_once('../Model/Value.php');
        $controller = new Value();

        
        echo json_encode($controller->createValue($datos[0], $datos[1], $datos[2]));

    break;

    case 'comment':

        include_once('../Model/Comments.php');
        $controller = new Comments();

        //$username,$parent_id,$comment,$id_post
        echo json_encode($controller->createComment($datos[0], $datos[1], $datos[2], $datos[3]));

    break;

    case 'tag':

        include_once('../Model/Tags.php');
        $controller = new Tags();

        //$username,$parent_id,$comment,$id_post
        echo json_encode($controller->createTag($datos[0], $datos[1], $datos[2], $datos[3]));

    break;

    default:

    break;
}
