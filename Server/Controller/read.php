<?php
header("Access-Control-Allow-Origin: http://localhost:4200");

$username = $_GET['username'];
$id = $_GET['id'];

switch ($_GET['object']) {

    case 'user':
        
        include_once('../Model/Users.php');
        $controller = new Users();

        
        if ($id == 'all') {

            echo $controller->returnAllUsers();
        } elseif ($username != '') {

            echo $controller->returnUser($username);
        }
        
        break;


    case 'post':
        
        include_once('../Model/Posts.php');
        $controller = new Posts();

        if ($id == 'all') {

            echo $controller->returnAllPost();
        } elseif($id == 'like'){
            var_dump($username);
            echo $controller->numlikes($username);

        } elseif ($id != '') {

            echo $controller->returnPost($id);
        } elseif ($username != '') {

            echo $controller->returnAllPostForUser($username);
        } 

        break;


    case 'w_posts':
        
        include_once('../Model/WaitingPosts.php');
        $controller = new WaitingPosts();

        if ($id == 'all') {
            
            echo $controller->returnWPosts();
        } elseif ($id != '') {
                
            echo $controller->returnWPostsId($id);
        } elseif ($username != '') {
           
            echo $controller->returnWPostsUser($username);
        }

        break;


    case 'value':
        
        include_once('../Model/Value.php');
        $controller = new Value();
    
        echo $controller->returnValue($username, $id);
    
        break;


    case 'comments':

        include_once('../Model/Comments.php');
        $controller = new Comments();
        
        if($username == 'commentsReply'){

            echo $controller->returnChildComments($id);
        }else{

            echo $controller->returnComment($id);
        }
        
        
        break;

    default:

        break;
}
