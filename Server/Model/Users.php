<?php
include_once('Queries.php');

class Users extends Queries
{


    public function createUsers($name, $birthDate, $username, $phone, $email, $pass)
    {

        if (json_decode($this->returnUser($username)) != "0 resultados") {
        } else {
            $sql = "INSERT INTO users (username, birthDate , name, phone, e_mail, password)
			values ('" . $username . "','" . $birthDate . "','" . $name . "'," . $phone . ",'" . $email . "','" . md5($pass) . "')";
            return Queries::insertDatas($sql);
        }
    }

    public function createUsersAdmin($name, $birthDate, $username, $phone, $email, $pass, $admin)
    {

        if (json_decode($this->returnUser($username)) != "0 resultados") {
        } else {
            $sql = "INSERT INTO users (username, birthDate , name, phone, e_mail, password, admin)
			values ('" . $username . "','" . $birthDate . "','" . $name . "'," . $phone . ",'" . $email . "','" . md5($pass) . "','" . $admin . "')";
            return Queries::insertDatas($sql);
        }
    }

    public function returnUser($username)
    {
        $sql = "SELECT * FROM users WHERE username = '$username' ";
        return Queries::returnDatas($sql);
    }

    public function returnAllUsers()
    {
        $sql = "SELECT * FROM users";
        return Queries::returnDatas($sql);
    }

    public function likeUser($username)
    {
        $sql = "SELECT * FROM users WHERE username LIKE '$username%' ";
        return Queries::returnDatas($sql);
    }

    public function numPost($username)
    {
        if ($this->returnUser($username) != "0 resultados") {
            $user = $this->returnUser($username);
            $user = json_decode($user);
            $user = $user[0]->numpost;
            $numPost = settype($user, "integer");
            $numPost++;
            var_dump($numPost);
            $sql = "UPDATE users SET numpost = '$numPost' WHERE username = '$username' ";
            Queries::insertDatas($sql);
        }
    }



    public function numPostDelete($username)
    {
        if ($this->returnUser($username) != "0 resultados") {
            $user = $this->returnUser($username);
            $user = json_decode($user);
            $user = $user[0]->numpost;
            $numPost = settype($user, "integer");
            $numPost = $numPost - 1;
            var_dump($numPost);
            $sql = "UPDATE users SET numpost = '$numPost' WHERE username = '$username' ";
            Queries::insertDatas($sql);
        }
    }

    public function likeFinalUser($username)
    {
        $sql = "SELECT * FROM users WHERE username = '$username' ";
        return Queries::returnDatas($sql);
    }

    public function deleteUser($username)
    {

        include_once('Comments.php');
        $comment = new Comments();
        if ($comment->returnCommentUser($username) != "0 resultados") {
            $comment->deleteCommentUsername($username);
        }


        include_once('Value.php');
        $value = new Value();
        if ($value->returnValueUser($username) != "0 resultados") {
            $value->deleteValueUser($username);
        }


        include_once('WaitingPosts.php');
        $waiting = new WaitingPosts();
        if ($waiting->returnWPosts($username) != "0 resultados") {
            $waiting->deleteWPostUser($username);
        }


        include_once('Posts.php');
        $post = new Posts();
        $postUser = $post->returnAllPostForUser($username);

        if ($postUser != "0 resultados") {
            $postUserDecode = json_decode($postUser);
            foreach ($postUserDecode as $infoPost) {
                print_r($infoPost->id);
                $post->deletePost($infoPost->id);
            }
        }

        if (json_decode($this->returnUser($username)) != "0 resultados") {
            $sql = "DELETE FROM users WHERE username = '$username'";
            Queries::insertDatas($sql);
        }
    }

    public function returnProfile($username)
    {
        $sql = "SELECT name, location, bio, birthDate FROM users WHERE username = '$username' ";
        return Queries::returnDatas($sql);
    }

    public function returnSettings($username)
    {
        $sql = "SELECT name, birthDate, phone, e_mail FROM users WHERE username = '$username' ";
        return Queries::returnDatas($sql);
    }

    public function updateEditProfile($name, $bio, $location, $base64, $image, $username)
    {
        if ($base64 == "UserNP") {
            $sql = "UPDATE users SET name = '$name', bio = '$bio', location = '$location', profile_image = '$image' WHERE username = '$username' ";
            Queries::insertDatas($sql);
        } else {
            $fp = fopen($image, 'w+');
            var_dump($image);
            $splitImagen = explode(',', $base64);
            fwrite($fp, base64_decode($splitImagen[1]));
            fclose($fp);
            $linkImagen = explode('/', $image);
            echo "<br>";
            echo "<br>";
            echo "<br>";
            $linkImagen = "https://picult.com/picult/" . $linkImagen[4] . "/" . $linkImagen[5] . "/" . $linkImagen[6] . "/" . $linkImagen[7] . "/" . $linkImagen[8] . $linkImagen[9];
            var_dump($linkImagen);


            $sql = "UPDATE users SET name = '$name', bio = '$bio', location = '$location', profile_image = '$linkImagen' WHERE username = '$username' ";
            Queries::insertDatas($sql);
        }
    }

    public function updateSettings($name, $birthDate, $username, $phone, $email)
    {
        if ($this->returnSettings($username) != "0 resultados") {
            $sql =  "UPDATE users SET name = '$name', birthDate = '$birthDate', phone = '$phone', e_mail = '$email' WHERE username = '$username'";
            Queries::insertDatas($sql);
        }
    }

    public function updateProfile($name, $location, $bio, $birthDate, $username)
    {
        if (json_decode($this->returnProfile($username)) != "0 resultados") {
            $sql =  "UPDATE users SET name = '$name', birthDate = '$birthDate', location = '$location', bio = '$bio' WHERE username = '$username'";
            return Queries::insertDatas($sql);
        }
    }

    //this.editUserForm.value.editName, this.editUserForm.value.editUsername, this.editUserForm.value.editBio,this.editUserForm.value.admin  

    public function UpdateAdmin($name, $username, $bio, $admin)
    {
        if (json_decode($this->returnProfile($username)) != "0 resultados") {
            $sql  = "UPDATE users SET name = '$name', bio = '$bio', admin = '$admin' WHERE username  = '$username'";
            Queries::insertDatas($sql);
        }
    }

    public function returnPassword($username)
    {
        $sql = "SELECT password FROM users WHERE username = '$username' ";
        return Queries::returnDatas($sql);
    }

    public function changePassword($username, $newPass, $oldPass)
    {

        if (json_decode($this->returnUser($username)) != "0 resultados") {

            $passCheck = $this->returnPassword($username);
            $passCheck = json_decode($passCheck);
            $beautifulCheck = "";


            foreach ($passCheck as $objectPass) {
                foreach ($objectPass as $stringPass) {
                    $beautifulCheck = $stringPass;
                }
            }


            if ($beautifulCheck == $oldPass) {
                $password = md5($newPass);
                $sql = "UPDATE users SET password = '$password' WHERE username = '$username'";
                Queries::insertDatas($sql);
            }
        }
    }

    public function login($username, $pass)
    {
        $passCheck = $this->returnPassword($username);
        $passCheck = json_decode($passCheck);

        if ($passCheck[0]->password == md5($pass)) {
            return true;
        } else {
            return false;
        }
    }

    public function returnProfileImage($username)
    {
        $sql = "SELECT * FROM users WHERE username = '$username' ";
        return Queries::returnDatas($sql);
    }


    public function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function forgetPassword($username, $email)
    {

        $userdata = json_decode($this->returnUser($username));

        if ($userdata != "0 resultados") {
            $sql = "SELECT e_mail FROM users WHERE username = '$username'";
            $query = json_decode(Queries::returnDatas($sql));

            if ($query != "0 resultados") {
                //var_dump($query[0]->e_mail);
               
                if ($query[0]->e_mail == $email) {
                    $message = "Line 1\r\nLine 2\r\nLine 3";

                    // In case any of our lines are larger than 70 characters, we should use wordwrap()
                    $message = wordwrap($message, 70, "\r\n");

                    // Send
                    mail($email, 'My Subject', $message);

                   /* include_once('./mail/src/PHPMailer.php');
                    include_once('./mail/src/SMTP.php');

                    $mail = new PHPMailer();
                    $mail->CharSet = 'UTF-8';

                    $body = 'Cuerpo del correo de prueba';

                    $mail->IsSMTP();
                    $mail->Host       = 'smtp.gmail.com';
                    $mail->SMTPSecure = 'tls';
                    $mail->Port       = 587;
                    $mail->SMTPDebug  = 1;
                    $mail->SMTPAuth   = true;
                    $mail->Username   = 'raunakbinyani.binyani@gmail.com';
                    $mail->Password   = '';
                    $mail->SetFrom('raunakbinyani.binyani@gmail.com', "rauu");
                    $mail->AddReplyTo('no-reply@mycomp.com', 'no-reply');
                    $mail->Subject    = 'Correo de prueba PHPMailer';
                    $mail->MsgHTML($body);

                    $mail->AddAddress($email, 'Gianni');
                    $mail->send();*/
                }
            }
        }
    }
}
