<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

    //LAS CONSULTAS DE LOS USUARIOS
    include_once('Users.php');

    /*
    //Prueba returnUser
    $username = "usuarioEncriptado";
    $result = new Users();
    $result->returnUser($username);
    */


    /*
    //Prueba createUsers
    $name = "Ala3valavencida";
    $birthDate = "0000-00-00";
    $username = "Ala3valavencida";
    $phone = "640230533";
    $email = "nuevo.com";
    $pass = "123";
    $result = new Users();
    $result->createUsers($name, $birthDate, $username, $phone, $email, $pass);
    */

	/*
    //Prueba deleteUser
    $username = "asier";
    $result = new Users();
    $result->deleteUser($username);
    */

    /*
    //Prueba returnProfile
    $username = "usuario3";
    $result = new Users();
    $result->returnProfile($username);
    */

    /*
    //Prueba returnSettings
    $username = "usuario3";
    $result = new Users();
    $result->returnSettings($username);
    */

    /*
    //Prueba updateSettings
    $username = "usuario3";
    $name = "señorUsuario";
    $birthDate = "0000-00-00";
    $phone = "640235693";
    $email = "nuevoCorreo";
    $result = new Users();
    $result->updateSettings($name, $birthDate, $username, $phone, $email);
    */

    /*
    //Prueba updateProfile
    $username = "usuario3";
    $name = "señorUsuario";
    $birthDate = "0000-00-00";
    $bio = "Soy el señor Usuario";
    $location = "ZonaUser";
    $result = new Users();
    $result->updateProfile($name, $location, $bio, $birthDate, $username);
    */

 

    /*
    //Prueba returnPassword
    $username = "usuarioEncriptado";
    $result = new Users();
    $result->returnPassword($username);
    */

    /*
    //Prueba changePassword
    $username = "Ala3valavencida";
    $newPass = "contraseñaEncriptada";
    $oldPass = "123";
    $result = new Users();
    $result->changePassword($username,$newPass,$oldPass);
    */


    //CONSULTAS DE POSTS
    include_once('Posts.php');

    
    //Prueba createPosts
    $username = "Asier123";
    $location = "AlgunSitio";
    $image = "https://picult.com/picult/Control/Server/images/posts/postDefault.jpg";
    $date = "0000-00-00";
    $description = "bababui";
    $tagName = "Playa";
    $result = new Posts();
    $result->createPost($username,$location,$image,$date,$description,$tagName);
    

    /*
    //Prueba deletePosts
    $id = 148;
    $result = new Posts();
    $result->deletePost($id);
    */
    
    /*
    //Prueba updatePost
    $averageRating = 0;
    $username = "ramon";
    $location = "localizaciónEditada";
    $image = "asfadsasfas";
    $id = 12;
    $dateApproved= "0000-00-00";
    $date = "0000-00-00";
    $description = "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rerum facilis quia nesciunt ducimus, dolore maiores cupiditate reprehenderit voluptas, accusantium aliquid quod eos totam nobis! Tempora praesentium quae fuga sit vero!";
    $result = new Posts();
    $result->updatePost($averageRating,$username,$location,$image,$id,$dateApproved,$date,$description,$result);
    */

    /*
    //Prueba returnPost
    $id = 12;
    $result = new Posts();
    $result->returnPost($id);
    */

    /*
    //Prueba returnAllPosts
    $result = new Posts();
    $result->returnAllPost();
    */

    /*
    //Prueba returnAllPostsForUser
    $username = "ramon";
    $result = new Posts();
    $result->returnAllPostForUser($username);
    */


    //CONSULTAS TAGS
    include_once('Tags.php');

    /*
    //Prueba createTag
    $name = "Playa";
    $result = new Tags();
    $result->createTag($name);
    */   

    /*
    //Prueba deleteTag    
    $name = "yoda";
    $result = new Tags();
    $result->deleteTag($name);
    */

    /*
    //Prueba updateTag
    $name = "papadopoulos";
    $newName = "holaCaracola";
    $result = new Tags();
    $result->updateTag($name,$newName);
    */

    /*
    //Prueba returnAllTag
    $result = new Tags();
    $result->returnAllTags();
    */


    /*$tagName = "345";
    $result = new Tags();
    $result->comprobateTag($tagName);
    */

    //CONSULTAS COMMENTS
    include_once('Comments.php');

   /* 
    //Prueba createComment
    $username = "ramon";
    $parent_id = 3;
    $id = 2;
    $date = "0000-00-00";
    $comment = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus enim earum nihil possimus incidunt dignissimos sequi impedit. Aliquid sit exercitationem repudiandae iusto magnam nemo nihil, quisquam voluptatum, doloribus natus commodi.";
    $result = new Comments();
    $result->createComment($username,$parent_id,$id,$date,$comment);
    */

    /*
    //Prueba deleteComment    
    $id = 2;
    $result = new Comments();
    $result->deleteComment($id);
    */

    /*    
    //Prueba updateComment
    $username = "asier";
    $parent_id = 3;
    $id = 2;
    $date = "0001-01-01";
    $comment = "Comentario de prueba UPDATE.";
    $result = new Comments();
    $result->updateComment($username,$parent_id,$id,$date,$comment);
    */

    /*
    //Prueba returnComment
    $id = 2;
    $result = new Comments();
    $result->returnComment($id);
    */

    /*
    //Prueba returnAllComments
    $idPost = 2;
    $result = new Comments();
    $result->returnAllComments($idPost);
    */

    //CONSULTAS DE WAITINGPOSTS
    include_once('WaitingPosts.php');

    /*
    //Prueba returnPosts
    $result = new WaitingPosts();
    $result->returnWPosts();
    */

    /*
    //Prueba acceptPost
    $username = "asier";
    $location = "AlgunSitio";
    $image = "post/204.jpg";
    $date = "0000-00-00";
    $description = "bababui";
    $tagName = "Playa";
    $dateApproved = date("d-m-Y H:i:s");
    $result = new WaitingPosts();
    $result->acceptWPost($username,$location,$image,$dateApproved,$date,$description,$tagName);
    */

    /*
    //Prueba deletePost
    $id = 24;
    $result = new WaitingPosts();
    $result->deletPost($id);
    */


    //CONSULTAS DE VALUE
    include_once('Value.php');
    /*
    //REVISAR
    //Prueba createValue
    $username = 'ramon';
    $id_post = 2;
    $valoration = 3;
    $result = new Value();
    $result->createValue($id_post,$username,$valoration);
    */

    /*
    //Prueba returnValue
    $result = new Value();
    $result->returnValue();
    */

    /*
    //Prueba returnValueUser
    $username = 'ramon';
    $result = new Value();
    $result->returnValueUser($username);
    */


    /*
    //Prueba returnValueId_post
    $id_post = '1';
    $result = new Value();
    $result->returnValueId_post($id_post);
    */

    /*
    //Prueba deleteValueUser
    $username = 'ramon';
    $result = new Value();
    $result->deleteValueUser($username);
    */

    /*
    //Prueba deleteValuePost
    $id_post = 2;
    $result = new Value();
    $result->deleteValuePost($id_post);
    */

    //CONSULTAS DE DESCRIPTION
    include_once('Description.php');

    /*
    //Prueba createDescrption
    $id_post = 8;
    $tagName = "cosa";
    $result = new Description();
    $result->createDescription($id_post,$tagName);
    */

    /*
    //Prueba returnDescription
    $result = new Description();
    $result->returnAllDescriptions();
    */

    /*
    //Prueba returnDescription
    $id_post = 8;
    $result = new Description();
    $result->returnDescriptionPost($id_post);
    */

    /*
    //Prueba deleteDescriptionPost
    $id_post = 8;
    $result = new Description();
    $result->deleteDescriptionPost($id_post);
    */


    /*
     //Prueba deleteDescriptionTagName
     $tagName = 8;
     $result = new Description();
     $result->deleteDescriptionTagName($tagName);
    */



    ?>
</body>

</html>