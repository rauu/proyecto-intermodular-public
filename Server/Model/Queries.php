<?php
include_once('Conexion.php');
class Queries
{
    public $conexion;

    public function automaticConexion()
    {
        $mvc_bd_conexion = new mysqli(Conexion::$mvc_bd_hostname, Conexion::$mvc_bd_usuario, Conexion::$mvc_bd_clave, Conexion::$mvc_bd_nombre);
        $error = $mvc_bd_conexion->connect_errno;
        if ($error != null) {
            echo "<p>Error " . $error . "conectando a la base de datos: ";
            echo $mvc_bd_conexion->connect_error . "</p>";
            exit();
        }
        $this->conexion = $mvc_bd_conexion;
    }

    protected function insertDatas($sql)
    {
        var_dump($sql);
        $this->automaticConexion();
        if (mysqli_query($this->conexion, $sql)) {
            echo "New record created successfully";
            return "New record created successfully";
        } else {
            echo  "Error: " . $sql . "<br>" . mysqli_error($this->conexion);
            return "Error: " . $sql . "<br>" . mysqli_error($this->conexion);
        }
        mysqli_close($this->conexion);
    }

    protected function returnDatas($sql)
    {
        
        $this->automaticConexion();
        $result = $this->conexion->query($sql);
        if ($result->num_rows > 0) {
            
            //Crea un array asociativo y mete todos los resultados en respuesta  
            for ($respuesta = array(); $row = $result->fetch_assoc(); $respuesta[] = $row);
            
        } else {
            $respuesta = "0 resultados";
        }
        //var_dump($respuesta);


        
        mysqli_close($this->conexion);
        if($respuesta == "0 resultados"){
            return json_encode($respuesta);
        }else{
            return json_encode($respuesta);
        }
    }

    protected function insertDatasTags($sql, $tag_Name, $username)
    {
        $this->automaticConexion();
        if (mysqli_query($this->conexion, $sql)) {

            if ($tag_Name != '') {
                include_once('Posts.php');
                $post = new Posts();
                $postUser = $post->returnAllPostForUser($username);
        
                $postUser = json_decode($postUser);
                $lastPost = array_pop($postUser);
                include_once('Description.php');
                $d = new Description();
                $tags = explode(" ", $tag_Name);
                
                foreach($tags as $tag){
                    echo 'x';
                    $d->createDescription($lastPost->id, $tag);
                }
            }

            
            echo "New record created successfully";
            return "New record created successfully";
        } else {
            echo  "Error: " . $sql . "<br>" . mysqli_error($this->conexion);
            return "Error: " . $sql . "<br>" . mysqli_error($this->conexion);
        }

        mysqli_close($this->conexion);
    }
}
