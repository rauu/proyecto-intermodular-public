<?php
    include_once('Queries.php');
    
    class Comments extends Queries
    {
        public function createComment($username,$parent_id,$comment,$id_post)
        {
            $sql = "INSERT INTO comments (username, parent_id, comment, id_post)
				values ('" . $username . "','" . $parent_id . "','" . $comment . "','" . $id_post . "')";
            return Queries::insertDatas($sql);
        }

        public function deleteComment($id)
        {
            if($this->returnComment($id) != "0 resultados")
            {
                $sql = "DELETE FROM comments WHERE id = '$id'";
                Queries::insertDatas($sql);
            }
            
        }

        public function deleteCommentUsername($username)
        {
            if($this->returnCommentUser($username) != "0 resultados")
            {
                $sql = "DELETE FROM comments WHERE username = '$username'";
                Queries::insertDatas($sql);
            }
            
        }

        public function deleteCommentPost($id_post)
        {
            if($this->returnAllComments($id_post) != "0 resultados")
            {
                $sql = "DELETE FROM comments WHERE id_post = '$id_post'";
                Queries::insertDatas($sql);
            }
            
        }

        public function updateComment($username,$parent_id,$id,$date,$comment)
        {
            $sql = "UPDATE comments SET username = '$username', parent_id = '$parent_id', date = '$date', comment = '$comment' WHERE id = $id ";
            Queries::insertDatas($sql);

        }

        public function returnComment($id)
        {
            $sql = "SELECT * FROM comments WHERE id_post = '$id'";
            return Queries::returnDatas($sql);
        }

        public function returnChildComments($id)
        {
            $sql = "SELECT * FROM comments WHERE parent_id = '$id'";
            return Queries::returnDatas($sql);
        }

        
        public function returnCommentUser($username)
        {
            $sql = "SELECT * FROM comments WHERE username = '$username'";
            return Queries::returnDatas($sql);
        }

       
        public function returnAllComments($idPost)
        {
            $sql = "SELECT * FROM comments, posts WHERE posts.id = $idPost  ";
            return Queries::returnDatas($sql);
        }

    }
?>