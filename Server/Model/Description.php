<?php
include_once('Queries.php');

class Description extends Queries
{
    public function returnAllDescriptions()
    {
        $sql = "SELECT * FROM description";
        return Queries::returnDatas($sql);
    }

    public function returnDescriptionPost($id_post)
    {
        $sql = "SELECT * FROM description WHERE id_post  = $id_post";
        return Queries::returnDatas($sql);
    }

    public function returnDescriptionTagName($tagName)
    {
        $sql = "SELECT id_post FROM description WHERE tagName_tag  LIKE '$tagName%'";
        return Queries::returnDatas($sql);
    }

    public function createDescription($id_post, $tagName_tag)
    {
        include_once("Tags.php");
        $tag = new Tags();
        $tag->comprobateTag($tagName_tag);
        $sql = "INSERT INTO description (id_post, tagName_tag )
        values (".$id_post.",'" . $tagName_tag . "')";
        Queries::insertDatas($sql);
        
    }

    public function deleteDescriptionPost($id_post)
    {
        if ($this->returnDescriptionPost($id_post) != "0 resultados") {
            $sql = "DELETE FROM description WHERE id_post = $id_post ";
            Queries::insertDatas($sql);
        }
    }

    public function deleteDescriptionTagName($id_post,$tagName_tag)
    {
        if ($this->returnDescriptionTagName($tagName_tag) != "0 resultados") {
            $sql = "DELETE FROM description WHERE tagName_tag = $tagName_tag ";
            Queries::insertDatas($sql);
        }
    }
}
