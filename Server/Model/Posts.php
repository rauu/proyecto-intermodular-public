<?php
include_once('Queries.php');

class Posts extends Queries
{

    public function createPost($username, $location, $image, $description, $tag_Name)
    {
        if (json_decode($this->returnPostImage($image)) == '0 resultados') {
            $sql = "INSERT INTO waiting_post (username, location, imageWP, descripcion, tagNames )
            values ('" . $username . "','" . $location . "','" . $image . "','" . $description . "','" . $tag_Name . "')";
            Queries::insertDatas($sql);
        }else{
            echo "Nombre ya existe";
        }
    }

    public function deletePost($id)
    {
        if (json_decode($this->returnPost($id)) != '0 resultados') {
            
            
            $datosPost = json_decode($this->returnPost($id));
            $username = $datosPost[0]->username;

            
            include_once('Users.php');
            $user = new Users();
             $user->numPostDelete($username);
            

            include_once('Description.php');
            $description = new Description();
            if ($description->returnDescriptionPost($id) != '0 resultados') {
                $description->deleteDescriptionPost($id);
            }

            include_once('Comments.php');
            $comment = new Comments();
            if ($comment->returnComment($id) != '0 resultados') {
                $comment->deleteCommentPost($id);
            }


            include_once('WaitingPosts.php');
            $waiting_post = new WaitingPosts();
            if ($waiting_post->returnWPostsId($id) != '0 resultados') {
                $waiting_post->deleteWPost($id);
            }
            if (json_decode($this->returnPost($id)) != '0 resultados') {
                $sql = "DELETE FROM posts WHERE id = $id";
                Queries::insertDatas($sql);
                
            }
            
        }
    }
    //Revisar que datos no queremos que se hagan update y como gestionarlo

    public function updatePost($id, $username, $date, $dateApproved, $location, $description, $image, $averageRating)
    {
        if ($this->returnPost($id) != '0 resultados') {
            $sql = "UPDATE posts SET valMedia = '$averageRating', username = '$username', location = '$location', imagen = '$image', dateApproved = '$dateApproved', fecha = '$date', descripcion = '$description'  WHERE id = '$id'";
            return Queries::insertDatas($sql);
        }
    }

    public function returnPost($id)
    {
        $sql = "SELECT * FROM posts WHERE id = '$id'";
        return Queries::returnDatas($sql);
    }

    public function returnAllPostForUser($username)
    {
        $sql = "SELECT * FROM posts WHERE username = '$username'";
        return Queries::returnDatas($sql);
    }

    public function returnAllPost()
    {
        $sql = 'SELECT * FROM posts';
        return Queries::returnDatas($sql);
    }

    public function generateRandomString($length = 30)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function returnPostImage($image)
    {
        $sql = "SELECT * FROM posts WHERE imagen = '$image'";
        return Queries::returnDatas($sql);
    }

    public function uploadPost($length, $imagen, $extension, $output_file = '/var/www/html/picult/Control/Server/images/waitingPosts/')
    {
        // open the output file for writing
        $nombreImagen = $output_file . $length . '.' . $extension;
        $fp = fopen($nombreImagen, 'w+');

        $splitImagen = explode(',', $imagen);
        fwrite($fp, base64_decode($splitImagen[1]));

        fclose($fp);
    }

    public function numlikes($username)
    {
        var_dump($username);
        $sql = "SELECT valMedia FROM posts where username = '.$username.'";
        $totalLikes = Queries::returnDatas($sql);
        $totalLikes = json_decode($totalLikes);
        return var_dump($totalLikes);
    }

}
